// --- Assignment 1 ---


// Should return a string with the longest word(s). Comma separated if there are several words
// with the same length
function longestWord(str) {
  // TODO: Implement
}

var s1 = "Hello js";
var s2 = "Hexagon1321 pie ---------";
var s3 = "";
var s4 = "hello mello my";
var s5 = "%#/ && !";
var s6 = "a b c d";


console.log("Assignment 1: ");
// Output should be: Longest word: Hello
console.log("Longest word: "+longestWord(s1));
// Output should be: Longest word: Hexagon
console.log("Longest word: "+longestWord(s2));
// Output should be: Longest word: 
console.log("Longest word: "+longestWord(s3));
// Output should be: Longest word: hello,mello
console.log("Longest word: "+longestWord(s4));
// Output should be: Longest word: 
console.log("Longest word: "+longestWord(s5));
// Output should be: Longest word: a,b,c,d
console.log("Longest word: "+longestWord(s6));


// --- Assignment 2 ---

// Returns a dictionary with the keys: street_name, street_number, house_number, appartment_number
function getAddressParts(s) {
  // TODO: Implement
  return {
    "street_name": "",
    "street_number": "",
    "house_number": "",
    "apartment_number": "",
  }
}

var streetAddress1 = "Lilla gatan 3A";
var streetAddress2 = "Slottet";
var streetAddress3 = "Stora blåa vägen 330";
var streetAddress4 = "Kungsgatan 3C lgh 1330";

console.log("Assignment 3: ");
// Output should be: Street parts: {"street_name":"Lilla gatan","street_number":"3","house_number":"A","apartment_number":""}
console.log("Street parts: "+JSON.stringify(getAddressParts(streetAddress1)));
// Output should be: Street parts: {"street_name":"Slottet","street_number":"","house_number":"","apartment_number":""}
console.log("Street parts: "+JSON.stringify(getAddressParts(streetAddress2)));
// Output should be: Street parts: {"street_name":"Stora blåa vägen","street_number":"330","house_number":"","apartment_number":""}
console.log("Street parts: "+JSON.stringify(getAddressParts(streetAddress3)));
// Output should be: Street parts: {"street_name":"Kungsgatan","street_number":"3","house_number":"C","apartment_number":"lgh 1330"}
console.log("Street parts: "+JSON.stringify(getAddressParts(streetAddress4)));


// --- Assignment 3 ---

// Should return a list with sorted integers
function sort(l) {
  // TODO: Implement
}

var integers1 = [];
var integers2 = ["9", "8", "1", "3", "5"];
var integers3 = ["321", "1", "100", "002", "001"];
var integers4 = ["Foo", "2000", "Baz", "100"];

console.log("Assignment 3: ");
// Output should be "Sorted integers: "
console.log("Sorted integers: "+sort(integers1));
// Output should be "Sorted integers: 1,3,5,8,9"
console.log("Sorted integers: "+sort(integers2));
// Output should be "Sorted integers: 1,1,2,100,321"
console.log("Sorted integers: "+sort(integers3));
// Output should be "Sorted integers: 100,2000"
console.log("Sorted integers: "+sort(integers4));
