# Uppgift 1
Implementera funktionerna som finns i filen `src/uppgift1.js` så att outputen blir enligt kommentarerna. Committa allt efterssom och pusha resultatet till ett eget git-repository (github eller gitlab exempelvis). Koden ska kunna köras med nodejs.

# Uppgfit 2
Använd `create-react-app` för att skapa en grundläggande React-app under katalogen react/
Skapa en ny komponent utöver den existerande App-komponenten. Komponenten ska renderas av App-komponenten och ta dagens datum som en prop från App-komponenten, exempelvis så här:
```javascript
<Date date={ the_date } />
```
Den nya komponenten ska bara skriva ut dagens datum.
Committa och pusha till samma repo.
